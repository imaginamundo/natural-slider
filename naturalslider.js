'use strict';

const NaturalSlider = {};

// helpers
NaturalSlider.insertAfter = function(newElement, referenceElement) {
    referenceElement.parentNode.insertBefore(newElement, referenceElement.nextSibling);
}

NaturalSlider.animate = function(duration = 150) {
    const position = this.scroll.element.scrollLeft;
    const requestAnimationFrame = window.requestAnimationFrame || 
                                window.mozRequestAnimationFrame || 
                                window.webkitRequestAnimationFrame || 
                                window.msRequestAnimationFrame;

    const tween = (start, end, duration) => {
        const delta = end - start;

        console.log(this.scroll);
        console.log('start', start);
        console.log('end', end);

        if (delta === 0) {
            this.scroll.animating = false;
            
            return;
        }

        const startTime = performance.now();

        const easeOutExpo = (t, b, c, d) => {
            return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
        }

        const tweenLoop = time => {
            const t = (!time ? 0 : time - startTime);
            const factor = easeOutExpo(t, 0, 1, duration);

            this.scroll.animating = true;

            if (t > duration) {
                this.scroll.element.scrollLeft = end;
                this.scroll.animating = false;
            }
            else {
                this.scroll.element.scrollLeft = start + delta * factor;
            }

            if (t < duration && this.scroll.scrollLeft != end) {
                requestAnimationFrame(tweenLoop);
            }
        }
        tweenLoop();
    }

    tween(position, this.scroll.position, 1000);
}

NaturalSlider.countBullets = function() {
    const scrollWidth = this.scroll.element.scrollWidth;
    const elementWidth = this.scroll.element.offsetWidth;

    let bulletsQuantity = scrollWidth / elementWidth;
        
    bulletsQuantity = Math.ceil(bulletsQuantity);
    
    this.scroll.bulletsQuantity = bulletsQuantity;

    return bulletsQuantity;
}

NaturalSlider.buildBullets = function() {
    this.scroll.bullets = document.createElement('div')
    this.scroll.bullets.classList.add('bullets');

    for (let i = 0; i < this.scroll.bulletsQuantity; i++) {
        const bulletButton = document.createElement('button');

        bulletButton.dataset.slideTo = i;

        this.selectBullet(bulletButton);

        this.scroll.bullets.append(bulletButton);
    }

    this.insertAfter(this.scroll.bullets, this.scroll.element);
}

NaturalSlider.updateBullets = function() {
    const newBullets = this.scroll.bulletsQuantity;
    const oldBullets = this.scroll.bullets.childElementCount;

    if (newBullets > oldBullets) {
        for (let i = oldBullets; i < newBullets; i++) {
            const bulletButton = document.createElement('button');

            bulletButton.dataset.slideTo = i;

            this.selectBullet(bulletButton);

            this.scroll.bullets.append(bulletButton);
        }
    }

    if (newBullets < oldBullets) {
        for (let i = newBullets; i < oldBullets; i++) {
            const deprecatedBullet = this.scroll.bullets.children[i];

            if (deprecatedBullet) {
                deprecatedBullet.remove();
            }
        }
    }
}

NaturalSlider.selectedBullet = function() {
    const width = this.scroll.width;
    const position = this.scroll.position;
    const activeBullet =  Math.round(position / width);

    if (activeBullet === -1) {
        activeBullet = 0;
    }
    
    if (activeBullet > this.scroll.bullets.childElementCount - 1) {
        activeBullet = this.scroll.bullets.childElementCount - 1;
    }

    for (let i = 0; i < this.scroll.bullets.childElementCount; i++) {
        this.scroll.bullets.children[i].classList.remove('active');
    }

    this.scroll.bullets.children[activeBullet].classList.add('active');
}

NaturalSlider.watchBoundries = function() {
    if (this.scroll.method === 'item') {
        const width = this.scroll.element.offsetWidth;
        const items = this.scroll.element.children;

        this.scroll.prevItem = 0;
        this.scroll.nextItem;

        for (let i = 0; i < items.length; i++) {
            const item = items[i],
                itemBoudry = this.positionRelativeToParent(this.scroll.element, item);

            // set previous item
            if (itemBoudry.left < -15) {
                this.scroll.prevItem = itemBoudry.left;
            }

            // set next item
            if (itemBoudry.right > 15) {
                this.scroll.nextItem = itemBoudry.right;

                i = items.left;
            }
        }
    }
}

NaturalSlider.watchScroll = function() {
    this.scroll.width = this.scroll.element.offsetWidth;
    this.scroll.position = this.scroll.element.scrollLeft;
    
    if (this.scroll.bullets) {
        this.scroll.bulletsQuantity = this.countBullets();

        this.updateBullets();
        this.selectedBullet();
    }
}

NaturalSlider.responsive = function() {
    this.scroll.width = this.scroll.element.offsetWidth;

    if (this.scroll.bullets) {
        this.scroll.bulletsQuantity = this.countBullets();

        this.updateBullets();
        this.selectedBullet();
    }
}

NaturalSlider.positionRelativeToParent = function(parent, child) {
    const parentBounds = parent.getBoundingClientRect(),
          childBounds = child.getBoundingClientRect(),
          relative = {};

    relative.top = childBounds.top - parentBounds.top,
    relative.right = childBounds.right - parentBounds.right,
    relative.bottom = childBounds.bottom - parentBounds.bottom,
    relative.left = childBounds.left - parentBounds.left;

    return relative;
}

NaturalSlider.build = function() {
    this.scroll.width =  this.scroll.element.offsetWidth;
    this.scroll.position = 0;

    if (this.scroll.bullets) {
        this.scroll.bullets = this.countBullets();

        this.buildBullets();
        this.selectedBullet();
    }

    if (this.scroll.nextButton) {
        this.scroll.nextButton.addEventListener('click', () => {
            NaturalSlider.navigationButtons('next');
        });
    }
    
    if (this.scroll.prevButton) {
        this.scroll.nextButton.addEventListener('click', () => {
            NaturalSlider.navigationButtons('prev');
        });
    }

    this.scroll.element.addEventListener('scroll', () => {
        this.watchScroll();
    });
}

// interaction
NaturalSlider.selectBullet = function(bullet) {
    bullet.addEventListener('click', (e) => {
        const bulletNumber = e.target.dataset.slideTo;
        const position = this.scroll.width * bulletNumber;

        this.scroll.position = position;

        this.selectedBullet();

        let animationVelocity = 150;

        if (this.scroll.method === 'item') {
            animationVelocity = animationVelocity / 3;
        }

        this.animate(animationVelocity);
    });

    this.scroll.width = this.scroll.element.offsetWidth;
}

NaturalSlider.navigationButtons = function(direction) {
    console.log(this);

    this.scroll.width = this.scroll.element.offsetWidth;
    this.scroll.position = this.scroll.element.scrollLeft;
    
    const width = this.scroll.element.offsetWidth;
    const position = this.scroll.position;

    let updatedPosition = 0;

    if (options.method === 'item') {
        this.watchBoundries();

        if (direction === 'next') {
            updatedPosition = this.scroll.nextItem;
        }
        if (direction === 'prev') {
            updatedPosition = this.scroll.prevItem;
        }

        updatedPosition = this.scroll.position + updatedPosition;

        if (updatedPosition > position - width) {
            updatedPosition = position - width;
        }

        if (updatedPosition < 0) {
            updatedPosition = 0;
        }
    }
    else {
        if (direction === 'next') {
            updatedPosition = this.scroll.position + width;

            if (updatedPosition > position - width) {
                updatedPosition = position - width;
            }
        }

        if (direction === 'prev') {
            updatedPosition = this.scroll.position - width;
            
            if (updatedPosition < 0) {
                updatedPosition = 0;
            }
        }
    }

    this.scroll.position = updatedPosition;

    let duration = 150;
    
    if (this.scroll.method === 'item') {
        duration = duration / 3;
    }

    this.animate(duration);
            
}

// init
NaturalSlider.init = function({
    element,
    bullets = false,
    method = 'block',
    nextButton = null,
    prevButton = null
}) {
    this.scroll = {
        element,
        bullets,
        method,
        nextButton,
        prevButton
    }

    this.build();

    window.addEventListener('resize', (e) => {
        this.responsive();
    });
}

const blockButtonsAndBullets = document.getElementById('block-buttons-bullets');
const scrollSampleOne = Object.create(NaturalSlider);
scrollSampleOne.init({
    element: blockButtonsAndBullets,
    bullets: true,
    nextButton: document.getElementById('block-buttons-bullets-next'),
    prevButton: document.getElementById('block-buttons-bullets-prev')
});

const blockAndBullets = document.getElementById('block-bullets');
const scrollSampleTwo = Object.create(NaturalSlider);
scrollSampleTwo.init({
    element: blockAndBullets,
    bullets: true
});

const itemAndBullets = document.getElementById('item-bullets');
const scrollSampleThree = Object.create(NaturalSlider);
scrollSampleThree.init({
    element: itemAndBullets,
    method: 'item',
    bullets: true
});

// Options
// key         | value             | default    | observation
// –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
// element     | DOM Element       | obligatory | Get specific slide with dataset.modal = 'specific'.
// method      | 'item' or 'block' | 'block'    | Block slide the whole width.
//                                              | Item will slide from item to item.
//                                              | Item will not create a bullet for item.
// bullets     | true or false     | false      | Add or remove bullets.
// prevButton  | DOM Element       | null       | Set element to go to prev stage on slider.
// nextButton  | DOM Element       | null       | Set element to go to next stage on slider.